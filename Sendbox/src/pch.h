#pragma once

// Librarys
#include <iostream>

// Data Structures
#include <string>
#include <vector>

// Utils
#include <Utils/Debug/Logger.h>
#include <Utils/Debug/DebugTimer.h>

using Logger = Utils::Debug::Logger;
using DebugTimer = Utils::Debug::DebugTimer;